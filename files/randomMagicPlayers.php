<?php

declare(strict_types=1);

// Tableaux
(array) $infoPlayers = [];
(array) $colorChoice = ['Noir', 'Rouge', 'Vert', 'Bleu', 'Blanc'];

// Variables
(int) $numberPlayers = -1;
(string) $inputName = '';
(int) $countNumberPlayers = 1;

// Message d'accueil
print(PHP_EOL . '///////////// Random Magic Players /////////////' . PHP_EOL);
print('* Script de tirage aléatoire pour les cartes Magic *' . PHP_EOL);

// Demande et stockage du nombre de joueurs
while (1 > $numberPlayers || 5 < $numberPlayers) {
    $numberPlayers++;
    print(PHP_EOL . 'Saisir le nombre de joueurs : ');
    $numberPlayers = intval(trim(fgets(STDIN)));
    if (1 > $numberPlayers || 5 < $numberPlayers) {
        print('1, 2, 3, 4 ou 5 joueurs !' . PHP_EOL);
    }
}

// Demande et stockage du nom des joueurs
for ($i = 0; $i < $numberPlayers; $i++) {
    $inputName = '';
    printf(PHP_EOL . 'Saisir le nom du joueur %d : ', $countNumberPlayers);
    $countNumberPlayers++;
    $inputName = strval(trim(fgets(STDIN)));
    $infoPlayers[] = $inputName;
}

// Initialisation des variables pour le tirage aléatoire
(string) $choiceP1 = $colorChoice[array_rand($colorChoice)];
(string) $choiceP2 = $colorChoice[array_rand($colorChoice)];
(string) $choiceP3 = $colorChoice[array_rand($colorChoice)];
(string) $choiceP4 = $colorChoice[array_rand($colorChoice)];
(string) $choiceP5 = $colorChoice[array_rand($colorChoice)];

// Message pour le résultat
print(PHP_EOL . '///////////// Résultat /////////////' . PHP_EOL);

// Switch case pour le nombre de joueurs
switch ($numberPlayers) {
        // Case pour 1 joueur
    case (1 == $numberPlayers):
        $nameP1 = $infoPlayers[0];
        (string) $choiceP1 = $colorChoice[array_rand($colorChoice)];
        printf(
            PHP_EOL . '- %s a la couleur %s' . PHP_EOL,
            $nameP1,
            $choiceP1
        );
        break;
        // Case pour 2 joueurs
    case (2 == $numberPlayers):
        $nameP1 = $infoPlayers[0];
        $nameP2 = $infoPlayers[1];
        (string) $choiceP1 = $colorChoice[array_rand($colorChoice)];
        while (!($choiceP2 != $choiceP1)) {
            (string) $choiceP2 = $colorChoice[array_rand($colorChoice)];
        }
        printf(
            PHP_EOL . '- %s a la couleur %s' . PHP_EOL . PHP_EOL . '- %s a la couleur %s' . PHP_EOL,
            $nameP1,
            $choiceP1,
            $nameP2,
            $choiceP2
        );
        break;
        // Case pour 3 joueurs
    case (3 == $numberPlayers):
        $nameP1 = $infoPlayers[0];
        $nameP2 = $infoPlayers[1];
        $nameP3 = $infoPlayers[2];
        (string) $choiceP1 = $colorChoice[array_rand($colorChoice)];
        while (!($choiceP2 != $choiceP1)) {
            (string) $choiceP2 = $colorChoice[array_rand($colorChoice)];
        }
        while (!($choiceP3 != $choiceP1 && $choiceP3 != $choiceP2)) {
            (string) $choiceP3 = $colorChoice[array_rand($colorChoice)];
        }
        printf(
            PHP_EOL . '- %s a la couleur %s' . PHP_EOL . PHP_EOL . '- %s a la couleur %s' . PHP_EOL . PHP_EOL . '- %s a la couleur %s' . PHP_EOL,
            $nameP1,
            $choiceP1,
            $nameP2,
            $choiceP2,
            $nameP3,
            $choiceP3
        );
        break;
        // Case pour 4 joueurs
    case (4 == $numberPlayers):
        $nameP1 = $infoPlayers[0];
        $nameP2 = $infoPlayers[1];
        $nameP3 = $infoPlayers[2];
        $nameP4 = $infoPlayers[3];
        (string) $choiceP1 = $colorChoice[array_rand($colorChoice)];
        while (!($choiceP2 != $choiceP1)) {
            (string) $choiceP2 = $colorChoice[array_rand($colorChoice)];
        }
        while (!($choiceP3 != $choiceP1 && $choiceP3 != $choiceP2)) {
            (string) $choiceP3 = $colorChoice[array_rand($colorChoice)];
        }
        while (!($choiceP4 != $choiceP1 && $choiceP4 != $choiceP2 && $choiceP4 != $choiceP3)) {
            (string) $choiceP4 = $colorChoice[array_rand($colorChoice)];
        }
        printf(
            PHP_EOL . '- %s a la couleur %s' . PHP_EOL . PHP_EOL . '- %s a la couleur %s' . PHP_EOL . PHP_EOL . '- %s a la couleur %s' . PHP_EOL . PHP_EOL . '- %s a la couleur %s' . PHP_EOL,
            $nameP1,
            $choiceP1,
            $nameP2,
            $choiceP2,
            $nameP3,
            $choiceP3,
            $nameP4,
            $choiceP4
        );
        break;
        // Default pour 5 joueurs
    default:
        $nameP1 = $infoPlayers[0];
        $nameP2 = $infoPlayers[1];
        $nameP3 = $infoPlayers[2];
        $nameP4 = $infoPlayers[3];
        $nameP5 = $infoPlayers[4];
        (string) $choiceP1 = $colorChoice[array_rand($colorChoice)];
        while (!($choiceP2 != $choiceP1)) {
            (string) $choiceP2 = $colorChoice[array_rand($colorChoice)];
        }
        while (!($choiceP3 != $choiceP1 && $choiceP3 != $choiceP2)) {
            (string) $choiceP3 = $colorChoice[array_rand($colorChoice)];
        }
        while (!($choiceP4 != $choiceP1 && $choiceP4 != $choiceP2 && $choiceP4 != $choiceP3)) {
            (string) $choiceP4 = $colorChoice[array_rand($colorChoice)];
        }
        while (!($choiceP5 != $choiceP1 && $choiceP5 != $choiceP2 && $choiceP5 != $choiceP3 && $choiceP5 != $choiceP4)) {
            (string) $choiceP5 = $colorChoice[array_rand($colorChoice)];
        }
        printf(
            PHP_EOL . '- %s a la couleur %s' . PHP_EOL . PHP_EOL . '- %s a la couleur %s' . PHP_EOL . PHP_EOL . '- %s a la couleur %s' . PHP_EOL . PHP_EOL . '- %s a la couleur %s' . PHP_EOL . PHP_EOL . '- %s a la couleur %s' . PHP_EOL,
            $nameP1,
            $choiceP1,
            $nameP2,
            $choiceP2,
            $nameP3,
            $choiceP3,
            $nameP4,
            $choiceP4,
            $nameP5,
            $choiceP5
        );
}
