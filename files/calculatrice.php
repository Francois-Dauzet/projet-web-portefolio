<?php

//////////////// Fonctions //////////////////

function sum($i, $j)
{
    return $i + $j;
}

function substraction($i, $j)
{
    return $i - $j;
}

function multiplication($i, $j)
{
    return $i * $j;
}

function division($i, $j)
{
    return $i / $j;
}

function numberCapture($message)
{
    do {
        print($message);
        $i = trim(fgets(STDIN));
    } while (!is_numeric($i));
    return $i;
}

function operatorInput()
{
    (string) $i = '';
    while (!($i == '+' || $i == '-' || $i == '/' || $i == '*')) {
        print('Saisir un opérateur ? ');
        $i = strval(trim(fgets(STDIN)));
    }
    return $i;
}

function results($j, $i, $k)
{
    switch ($i) {
        case ('+'):
            return sum($j, $k);
            break;
        case ('-'):
            return substraction($j, $k);
            break;
        case ('/'):
            return division($j, $k);
            break;
        default:
            return multiplication($j, $k);
    }
}

//////////////// Variables //////////////////

(bool) $endEntry = true;
(string) $endChoice = '';

while ($endEntry == true) {

    (int) $numberCaptureA = 0;
    (int) $numberCaptureB = 0;
    (string) $operatorInput = '';
    (int) $results = 0;
    (int) $calculationChoice = 0;
    (string) $totalCapture = '';
    (int) $calculationChoiceAri = 0;

    ////////////////// Tableaux //////////////////

    $stockFormula = [];

    ///////////////// Saisie /////////////////

    do {
        print(PHP_EOL . '1 = Calculs arithmétiques / 2 = Calculs de surface' . PHP_EOL . 'Faites votre choix ? ');
        $calculationChoice = trim(fgets(STDIN));
    } while (!is_numeric($calculationChoice));

    if (1 == $calculationChoice) {

        print(PHP_EOL . PHP_EOL . '//////////' . 'Calculs arithmétiques' . '//////////' . PHP_EOL);

        do {
            print(PHP_EOL . '1 = Version simple / 2 = Version bonus / 3 = Version bonus 2.0' . PHP_EOL . 'Faites votre choix ? ');
            $calculationChoiceAri = trim(fgets(STDIN));
        } while (!is_numeric($calculationChoiceAri));

        ///////////////// Version Bonus /////////////////

        if (2 == $calculationChoiceAri) {
            print(PHP_EOL . PHP_EOL . '//////////' . 'Version bonus' . '//////////' . PHP_EOL);
            print('Formule de calcul (x + y) ? ');
            $totalCapture = strval(trim(fgets(STDIN)));

            $stockFormula[] = explode(' ', $totalCapture);

            $numberCaptureA = $stockFormula[0][0];
            $operatorInput = $stockFormula[0][1];
            $numberCaptureB = $stockFormula[0][2];

            $results = results($numberCaptureA, $operatorInput, $numberCaptureB);

            print(PHP_EOL . 'Résultat = ' . $results . PHP_EOL . PHP_EOL);

            ///////////////// Version Bonus V2 /////////////////

        } else if (3 == $calculationChoiceAri) {
            print(PHP_EOL . PHP_EOL . '//////////' . 'Version bonus V2' . '//////////' . PHP_EOL);

            print(PHP_EOL . 'Formule de calcul ? ');
            $totalCapture = strval(trim(fgets(STDIN)));


            $operator = "";
            $add = "+";
            $sou = "-";
            $mul = "*";
            $div = "/";

            switch ($totalCapture) {
                case (strpos($totalCapture, $add) !== false):
                    $stockFormula[] = explode('+', $totalCapture, 3);
                    $operatorInput = $add;
                    break;
                case (strpos($totalCapture, $sou) !== false):
                    $stockFormula[] = explode('-', $totalCapture, 3);
                    $operatorInput = $sou;
                    break;
                case (strpos($totalCapture, $mul) !== false):
                    $stockFormula[] = explode('*', $totalCapture, 3);
                    $operatorInput = $mul;
                    break;
                case (strpos($totalCapture, $div) !== false):
                    $stockFormula[] = explode('/', $totalCapture, 3);
                    $operatorInput = $div;
                    break;
            }

            $numberCaptureA = $stockFormula[0][0];
            $numberCaptureB = $stockFormula[0][1];

            $results = results($numberCaptureA, $operatorInput, $numberCaptureB);

            print(PHP_EOL . 'Résultat = ' . $results . PHP_EOL . PHP_EOL);

            ///////////////// Version Simple /////////////////

        } else {
            print(PHP_EOL . PHP_EOL . '//////////' . 'Version simple' . '//////////' . PHP_EOL . PHP_EOL);

            $numberCaptureA = numberCapture('Saisie d\'un nombre réel ? ');
            $operatorInput = operatorInput();
            $numberCaptureB = numberCapture('Saisie d\'un nombre réel ? ');

            $results = results($numberCaptureA, $operatorInput, $numberCaptureB);

            print(PHP_EOL . 'Résultat = ' . $results . PHP_EOL . PHP_EOL);
        }

        ///////////////// Calcul de surface /////////////////

    } else {
        print(PHP_EOL . PHP_EOL . '//////////' . 'Calculs de surface' . '//////////' . PHP_EOL . PHP_EOL);

        $numberCaptureA = numberCapture('Saisie de la largeur ? ');

        $numberCaptureB = numberCapture('Saisie de la longueur ? ');

        $results = multiplication($numberCaptureA, $numberCaptureB);
        print(PHP_EOL . 'La surface totale est de ' . $results . PHP_EOL . PHP_EOL);
    }

    while ($endChoice != 'oui' && $endChoice != 'non') {
        print(PHP_EOL . 'Avez vous d\'autres calculs à faire ("oui" ou "non") ? ');
        $endChoice = strval(trim(fgets(STDIN)));
    }

    $endEntry = ($endChoice == 'oui');
}
