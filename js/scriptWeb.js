const btnLeft = document.querySelector("#btnLeft");
const btnRight = document.querySelector("#btnRight");

let countSlide = 93;
let totalSlide = 99;

btnLeft.addEventListener("click", () => {
  if (countSlide > 93) {
    setTimeout(() => {
      document.querySelector(".leftArrow").style.display = "none";
      document.querySelector(".rightArrow").style.display = "none";
    }, 5);

    document
      .querySelector(".slide" + countSlide)
      .classList.remove("hidden-left");
    document
      .querySelector(".slide" + countSlide)
      .classList.remove("show-slide");
    document.querySelector(".slide" + countSlide).classList.add("hidden-right");
    setTimeout(() => {
      document.querySelector(".slide" + countSlide).style.display = "none";
      document
        .querySelector(".slide" + countSlide)
        .classList.remove("hidden-right");
      countSlide--;
      document
        .querySelector(".slide" + countSlide)
        .classList.add("hidden-left");
      document.querySelector(".slide" + countSlide).style.display = "flex";
    }, 500);
    setTimeout(() => {
      document.querySelector(".slide" + countSlide).classList.add("show-slide");
    }, 520);
    setTimeout(() => {
      document.querySelector(".leftArrow").style.display = "flex";
      document.querySelector(".rightArrow").style.display = "flex";
    }, 800);
  }
});

btnRight.addEventListener("click", () => {
  if (countSlide < totalSlide) {
    setTimeout(() => {
      document.querySelector(".leftArrow").style.display = "none";
      document.querySelector(".rightArrow").style.display = "none";
    }, 5);
    document
      .querySelector(".slide" + countSlide)
      .classList.remove("hidden-right");
    document
      .querySelector(".slide" + countSlide)
      .classList.remove("show-slide");
    document.querySelector(".slide" + countSlide).classList.add("hidden-left");
    setTimeout(() => {
      document.querySelector(".slide" + countSlide).style.display = "none";
      document
        .querySelector(".slide" + countSlide)
        .classList.remove("hidden-left");
      countSlide++;
      document
        .querySelector(".slide" + countSlide)
        .classList.add("hidden-right");
      document.querySelector(".slide" + countSlide).style.display = "flex";
    }, 500);
    setTimeout(() => {
      document.querySelector(".slide" + countSlide).classList.add("show-slide");
    }, 520);
    setTimeout(() => {
      document.querySelector(".leftArrow").style.display = "flex";
      document.querySelector(".rightArrow").style.display = "flex";
    }, 800);
  }
});
